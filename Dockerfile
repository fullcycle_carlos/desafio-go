FROM golang:1.17.1 as builder

WORKDIR /opt/app

COPY . .

RUN go build main.go

FROM scratch

COPY --from=builder /opt/app/main .

CMD ["./main"]
